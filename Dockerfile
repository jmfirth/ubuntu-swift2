FROM ubuntu:14.04.3
MAINTAINER Justin Firth <jmfirth@gmail.com>

RUN \
	apt-get update && \
	apt-get upgrade -y && \
	apt-get install -y build-essential clang-3.6 uuid-dev libicu-dev icu-devtools libbsd-dev libedit-dev libxml2-dev libsqlite3-dev libpython-dev libncurses5-dev curl rsync && \
	update-alternatives --install /usr/bin/clang clang /usr/bin/clang-3.6 100 && \
	update-alternatives --install /usr/bin/clang++ clang++ /usr/bin/clang++-3.6 100

RUN \
	curl https://swift.org/builds/ubuntu1404/swift-2.2-SNAPSHOT-2015-12-01-b/swift-2.2-SNAPSHOT-2015-12-01-b-ubuntu14.04.tar.gz --output swift.tar.gz && \
	tar xvzf swift.tar.gz && \
	rsync -av swift-2.2-SNAPSHOT-2015-12-01-b-ubuntu14.04/usr/ /usr && \
	rm -rf swift-2.2-SNAPSHOT-2015-12-01-b-ubuntu14.04
