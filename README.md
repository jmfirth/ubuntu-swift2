# jfirth/ubuntu-swift2

> Swift in an Ubuntu 14.04.3 Docker

## Build Image

```
$ docker build -t jfirth/ubuntu-swift2 .
```

## Run Ephemeral REPL

```
$ docker run -it --rm jfirth/ubuntu-swift2 swift
```
